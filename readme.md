# Minecraft 1.16.5 Fabric, JRE8

Эта версия майнкрафта является последней, работающей под jre8 и потребляющей
всего 850 мегабайт памяти со старта (без модов). Версии посвежее требуют
последнюю jre (на данный момент 19) и **гораздо** больше памяти (около 2 Гб со
старта, а в процессе игры больше четырёх).

## Моды первой необходимости

- [x] [Fabric loader](https://fabricmc.net) `0.14.12`
- [x] [Fabric API](https://www.curseforge.com/minecraft/mc-mods/fabric-api/files/all?filter-game-version=2020709689%3A8203)
      `0.42.0`
- [x] [Sodium](https://www.curseforge.com/minecraft/mc-mods/sodium/files/all?filter-game-version=2020709689%3A8203)
      `0.2.0`
- [x] [Lithium](https://www.curseforge.com/minecraft/mc-mods/lithium/files/all?filter-game-version=2020709689%3A8203)
      `0.6.6`
- [x] [Phosphor](https://www.curseforge.com/minecraft/mc-mods/phosphor/files/all?filter-game-version=2020709689%3A8203)
      `0.8.0`
- [x] [Cloth Config API](https://www.curseforge.com/minecraft/mc-mods/cloth-config/files/all?filter-game-version=2020709689%3A8203)
      `4.16.91`
- [x] [Mod Menu](https://www.curseforge.com/minecraft/mc-mods/modmenu/files/all?filter-game-version=2020709689%3A8203)
      `1.16.23`

## Крутые, стабильные

- [x] [RPG-Hud](https://www.curseforge.com/minecraft/mc-mods/rpg-hud-fabric/files/all?filter-game-version=2020709689%3A8203)
      `3.8.1`
- [x] [Charm](https://www.curseforge.com/minecraft/mc-mods/charm/files/all?filter-game-version=2020709689%3A8203)
      `2.3.2`
- [x] [AppleSkin](https://www.curseforge.com/minecraft/mc-mods/appleskin/files/all?filter-game-version=2020709689%3A8203)
      `2.4.0`
- [x] [Fabric Waystones](https://www.curseforge.com/minecraft/mc-mods/fabric-waystones/files/all?filter-game-version=2020709689%3A8203)
      `2.0.2`

## Крутые, но не жизненно важные

- [ ] [Inventory Profiles](https://www.curseforge.com/minecraft/mc-mods/inventory-profiles/files/all?filter-game-version=2020709689%3A8203)
- [x] [FallingTree](https://www.curseforge.com/minecraft/mc-mods/falling-tree/files/all?filter-game-version=2020709689%3A8203)
      `2.11.6`
- [ ] [Xaero's Minimap](https://www.curseforge.com/minecraft/mc-mods/xaeros-minimap/files/all?filter-game-version=2020709689%3A8203)
- [ ] [REI](https://www.curseforge.com/minecraft/mc-mods/roughly-enough-items/files/all?filter-game-version=2020709689%3A8203)
      `6.5.436`
  - [ ] [Architectury API](https://www.curseforge.com/minecraft/mc-mods/architectury-api/files/all?filter-game-version=2020709689%3A8203)
        `1.32.66`
- [x] [Tom's Simple Storage Mod](https://www.curseforge.com/minecraft/mc-mods/toms-storage-fabric/files/all?filter-game-version=2020709689%3A8203)
      `1.1.16`
- [x] [Eldritch Mobs](https://www.curseforge.com/minecraft/mc-mods/eldritch-mobs/files/all?filter-game-version=2020709689%3A8203)
      `1.8.0`
- [x] [Gravestones](https://www.curseforge.com/minecraft/mc-mods/gravestones/files/all?filter-game-version=2020709689%3A8203)
      `1.9`
- [ ] [Where Is It](https://www.curseforge.com/minecraft/mc-mods/where-is-it/files/all?filter-game-version=2020709689%3A8203)
- [ ] [CleanCut](https://www.curseforge.com/minecraft/mc-mods/cleancut/files/all?filter-game-version=2020709689%3A8203)
      `1.16-2.2`

## Контент

### Приключения

- [ ] [Aether](https://www.curseforge.com/minecraft/mc-mods/aether/files/all?filter-game-version=2020709689%3A8203)
- [ ] [AdventureZ](https://www.curseforge.com/minecraft/mc-mods/adventurez/files/all?filter-game-version=2020709689%3A8203)
- [ ] [MobZ](https://www.curseforge.com/minecraft/mc-mods/mobz/files/all?filter-game-version=2020709689%3A8203)
- [ ] [Dungeons Mod Lite](https://www.curseforge.com/minecraft/mc-mods/dungeons-mod-lite/files/all?filter-game-version=2020709689%3A8203)
- [ ] [Castle Dungeons](https://www.curseforge.com/minecraft/mc-mods/castle-dungeons/files/all?filter-game-version=2020709689%3A8203)

### Биомы и структуры

- [ ] [YUNG's API](https://www.curseforge.com/minecraft/mc-mods/yungs-api-fabric/files/all?filter-game-version=2020709689%3A8203)
  - [ ] [Better Strongholds](https://www.curseforge.com/minecraft/mc-mods/yungs-better-strongholds/files/all?filter-game-version=2020709689%3A8203)
  - [ ] [Better Mineshafts](https://www.curseforge.com/minecraft/mc-mods/yungs-better-mineshafts-fabric/files/all?filter-game-version=2020709689%3A8203)
  - [ ] [BetterEnd](https://www.curseforge.com/minecraft/mc-mods/betterend/files/all?filter-game-version=2020709689%3A8203) -
        experimental
  - [ ] [Better Caves](https://www.curseforge.com/minecraft/mc-mods/yungs-better-caves-fabric/files/all?filter-game-version=2020709689%3A8203)
- [ ] [Oh The Biomes You'll Go](https://www.curseforge.com/minecraft/mc-mods/oh-the-biomes-youll-go-fabric/files/all?filter-game-version=2020709689%3A8203) -
      experimental
- [ ] [Biome Makeover](https://www.curseforge.com/minecraft/mc-mods/biome-makeover/files/all?filter-game-version=2020709689%3A8203)
- [ ] [Repurposed Structures](https://www.curseforge.com/minecraft/mc-mods/repurposed-structures-fabric/files/all?filter-game-version=2020709689%3A8203)
- [ ] [Cave Biomes](https://www.curseforge.com/minecraft/mc-mods/cave-biomes/files/all?filter-game-version=2020709689%3A8203)
- [ ] [Wild World](https://www.curseforge.com/minecraft/mc-mods/wild-world/files/all?filter-game-version=2020709689%3A8203)
- [ ] [Mo' Structures](https://www.curseforge.com/minecraft/mc-mods/mo-structures/files/all?filter-game-version=2020709689%3A8203)
- [ ] [Cinderscapes](https://www.curseforge.com/minecraft/mc-mods/cinderscapes/files/all?filter-game-version=2020709689%3A8203)
- [ ] [Traverse](https://www.curseforge.com/minecraft/mc-mods/traverse/files/all?filter-game-version=2020709689%3A8203)
- [ ] [Terrestria](https://www.curseforge.com/minecraft/mc-mods/terrestria/files/all?filter-game-version=2020709689%3A8203)

## Запасные

- [ ] [Earth2Java](https://www.curseforge.com/minecraft/mc-mods/earth2java-fabric/files/all?filter-game-version=2020709689%3A8203)
- [ ] [Rat's Mischief](https://www.curseforge.com/minecraft/mc-mods/rats-mischief/files/all?filter-game-version=2020709689%3A8203)
- [ ] [Untitled Duck](https://www.curseforge.com/minecraft/mc-mods/untitled-duck-mod-fabric/files/all?filter-game-version=2020709689%3A8203)
  - [ ] [GeckoLib](https://www.curseforge.com/minecraft/mc-mods/geckolib-fabric/files/all?filter-game-version=2020709689%3A8203)
- [ ] [Direbats](https://www.curseforge.com/minecraft/mc-mods/direbats-fabric/files/all?filter-game-version=2020709689%3A8203)
- [ ] [Monster Of The Ocean Depths](https://www.curseforge.com/minecraft/mc-mods/monster-of-the-ocean-depths-fabric/files/all?filter-game-version=2020709689%3A8203)
- [ ] [The Parakeet](https://www.curseforge.com/minecraft/mc-mods/the-parakeet-mod/files/all?filter-game-version=2020709689%3A8203)
- [ ] [Alaska Native Craft](https://www.curseforge.com/minecraft/mc-mods/alaska-native-craft/files/all?filter-game-version=2020709689%3A8203)
- [ ] [Cheeky Monkeys](https://www.curseforge.com/minecraft/mc-mods/cheeky-monkeys/files/all?filter-game-version=2020709689%3A8203)
- [ ] [Chicken Drop Feathers](https://www.curseforge.com/minecraft/mc-mods/chicken-drop-feathers/files/all?filter-game-version=2020709689%3A8203)
- [ ] [Extra Alchemy](https://www.curseforge.com/minecraft/mc-mods/extra-alchemy/files/all?filter-game-version=2020709689%3A8203)
- [ ] [Croptopia](https://www.curseforge.com/minecraft/mc-mods/croptopia-fabric/files/all?filter-game-version=2020709689%3A8203)
- [ ] [Artifacts](https://www.curseforge.com/minecraft/mc-mods/artifacts-fabric/files/all?filter-game-version=2020709689%3A8203)
  - [ ] [Trinkets](https://www.curseforge.com/minecraft/mc-mods/trinkets-fabric/files/all?filter-game-version=2020709689%3A8203)
- [ ] [Patchouli](https://www.curseforge.com/minecraft/mc-mods/patchouli-fabric/files/all?filter-game-version=2020709689%3A8203)
- [ ] [NoFog](https://www.curseforge.com/minecraft/mc-mods/nofog/files/all?filter-game-version=2020709689%3A8203)
- [ ] [Clear Skies](https://www.curseforge.com/minecraft/mc-mods/clear-skies/files/all?filter-game-version=2020709689%3A8203)
- [ ] [Presence Footsteps](https://www.curseforge.com/minecraft/mc-mods/presence-footsteps/files/all?filter-game-version=2020709689%3A8203)
- [ ] [Kotlin](https://www.curseforge.com/minecraft/mc-mods/fabric-language-kotlin/files/all?filter-game-version=2020709689%3A8203)
  - [ ] [Woods and Mires](https://www.curseforge.com/minecraft/mc-mods/woods-and-mires/files/all?filter-game-version=2020709689%3A8203)
  - [ ] [Gentle Fawn](https://www.curseforge.com/minecraft/mc-mods/gentle-fawn/files/all?filter-game-version=2020709689%3A8203)
  - [ ] [Adorn](https://www.curseforge.com/minecraft/mc-mods/adorn/files/all?filter-game-version=2020709689%3A8203)
  - [ ] [Flamingo](https://www.curseforge.com/minecraft/mc-mods/flamingo-oh-oh-oh/files/all?filter-game-version=2020709689%3A8203)
- [ ] [Physics Mod](https://www.curseforge.com/minecraft/mc-mods/physics-mod/files/all?filter-game-version=2020709689%3A8203)
- [ ] [WTHIT](https://www.curseforge.com/minecraft/mc-mods/wthit/files/all?filter-game-version=2020709689%3A8203)
- [ ] [Antique Atlas](https://www.curseforge.com/minecraft/mc-mods/antique-atlas/files/all?filter-game-version=2020709689%3A8203)
- [ ] [slotlink](https://www.curseforge.com/minecraft/mc-mods/slotlink/files/all?filter-game-version=2020709689%3A8203)
- [ ] [OptiFabric](https://www.curseforge.com/minecraft/mc-mods/optifabric/files/all?filter-game-version=2020709689%3A8203)
  - [ ] [OptiFine](https://www.optifine.net/downloads)
- [ ] [Immersive Portals](https://www.curseforge.com/minecraft/mc-mods/immersive-portals-mod/files/all?filter-game-version=2020709689%3A8203)

## Не проверенные
